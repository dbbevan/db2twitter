How to install db2twitter
=========================
From sources
^^^^^^^^^^^^
From db2twitter 0.6, you will need the `Pillow imaging library <https://python-pillow.org/>`_ in order to send images to Twitter. On a Debian system to install Pillow you could need the libjpeg-dev dependency. Install it with the following command:

    # apt-get install libjpeg-dev

From PyPI
^^^^^^^^^
    $ pip3 install db2twitter

From sources
^^^^^^^^^^^^
* You need at least Python 3.5.
* Untar the tarball and go to the source directory with the following commands::

    $ tar zxvf db2twitter-0.10.tar.gz
    $ cd db2twitter

* Next, to install db2twitter on your computer, type the following command with the root user::

    $ python3 setup.py install
    $ # or
    $ python3 setup.py install --install-scripts=/usr/bin

