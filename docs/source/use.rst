Use db2twitter
==============
After the configuration of db2twitter, just launch the following command in order to populate your data without posting them:

    $ db2twitter --populate /path/to/db2twitter.ini

After the first execution, you will use the following command to post the new messages:

    $ db2twitter /path/to/db2twitter.ini

We recommend using db2twitter with cron. The following line in /etc/crontab will check for new db rows in your database every minute, build and send tweets accordingly::

    # m h dom mon dow user  command
    * * * * * db2twitter db2twitter /path/to/db2twitter.ini
